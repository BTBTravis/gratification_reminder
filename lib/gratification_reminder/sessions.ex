defmodule GratificationReminder.Sessions do
  @moduledoc """
  The Session context.
  """
  alias GratificationReminder.Accounts, as: Accounts
  alias GratificationReminder.Sessions.Session, as: Session

  def create_session(raw_data) do
    with {:ok, user} <- get_user(raw_data),
	 {:ok, message} <- get_message(raw_data),
	 {:ok, chat_id} <- get_chat_id(raw_data)
    do
      {:ok, %Session{
        user: user,
        chat_id: chat_id,
        message: message,
        responces: [],
      }}
    end
  end

  def add_message(session, msg) do
    %Session{session | :responces => session.responces ++ [msg]}
  end

  defp get_message(raw_data) do
    case get_in(raw_data, ["message", "text"]) do
      nil -> {:error, "can't find message in raw_data"}
      x -> {:ok, x}
    end
  end

  defp get_user_id(raw_data) do
    case get_in(raw_data, ["message", "from", "id"]) do
      nil -> {:error, "can't find user_id in raw_data"}
      x -> {:ok, x}
    end
  end

  @doc """
  Gets a user object or creates a new one as needed
  """
  defp get_user(raw_data) do
    with {:ok, id_from_message} <- get_user_id(raw_data) do
      case Accounts.get_user_by_telegram_id(id_from_message) do
        nil -> Accounts.create_user(%{telegram_id: id_from_message})
        user -> {:ok, user}
      end
    end
  end

  defp get_chat_id(raw_data) do
    case get_in(raw_data, ["message", "chat", "id"]) do
      nil -> {:error, "can't find user_id in raw_data"}
      x -> {:ok, x}
    end
  end
end
