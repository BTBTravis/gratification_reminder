defmodule GratificationReminder.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias GratificationReminder.Notes.Note

  schema "users" do
    field :telegram_id, :integer
    field :is_deleted, :boolean
    has_many :notes, Note

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:telegram_id, :is_deleted])
    |> validate_required([:telegram_id])
    |> unique_constraint(:telegram_id)
  end
end
