defmodule GratificationReminder.Notes.Note do
  use Ecto.Schema
  import Ecto.Changeset
  alias GratificationReminder.Accounts.User


  schema "notes" do
    field :note, :string
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(note, attrs) do
    note
    |> cast(attrs, [:note])
    |> validate_required([:note])
  end
end
