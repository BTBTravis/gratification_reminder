defmodule GratificationReminder.Repo do
  use Ecto.Repo,
    otp_app: :gratification_reminder,
    adapter: Ecto.Adapters.Postgres
end
