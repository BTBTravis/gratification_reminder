defmodule GratificationReminder.Sessions.Session do
  @enforce_keys [:user, :chat_id, :message]
  defstruct [:user, :chat_id, :message, :responces]
end
