defmodule GratificationReminder.Bot do
  alias GratificationReminder.{Sessions, Outbox, Notes, Accounts}

  def handle_incoming_telegram_msg(raw_data) do
    case Sessions.create_session(raw_data) do
      {:ok, session} -> discern_intent(session)
        |> Outbox.handle_session
      x -> x
    end
  end

  defp discern_intent(session) do
    cond do
      Regex.match?(~r/^\/!forgetme/, session.message) -> forget_user(session)
      Regex.match?(~r/^\/forgetme/, session.message) -> warn_forget_user(session)
      Regex.match?(~r/^\/id/, session.message) -> send_id(session)
      Regex.match?(~r/^\/save/, session.message) -> save_note(session)
      Regex.match?(~r/^\/(start|help)/, session.message) -> send_help_msg(session)
      true -> Sessions.add_message(session, "save what you are thankful for with ex /save the color of the sky today")
    end
  end

  defp forget_user(session) do
    case Accounts.delete_user(session.user) do
      {:ok, _} -> Sessions.add_message(session, "Deleted your user thanks for using Gratification Bot")
      _ -> Sessions.add_message(session, "Sorry there was a problem deleting your user please try again later")
    end
  end

  defp warn_forget_user(session) do
      Sessions.add_message(session, """
        Are you sure? We will delete all information we have on you and reset.
        If you are sure this is what you want please send back /!forgetme
        """)
  end

  defp send_id(session) do
    Sessions.add_message(session, "Your user id is: #{session.user.id}")
  end

  defp save_note(session) do
    with matches <- Regex.run(~r/^\/save\W+(.+)/, session.message),
         {:ok, note} <- (if !is_nil(matches), do: Enum.fetch(matches, 1), else: nil) do
      case Notes.create_note(session.user, %{note: note}) do
        {:ok, n} -> Sessions.add_message(session, """
          Saved your note successfully
          "#{n.note}"
        """)
        _ -> Sessions.add_message(session, "Shits, problem saving saving your note, contact travis")
      end
    else
      _ -> Sessions.add_message(session, "Please send note in the form of /save the thing I'm thankful for")
    end
  end

  defp send_help_msg(session) do
    Sessions.add_message(session, """
      Hello I'm a bot assistant that helps remind you of what you are thankful.
      Simply send me thing that you are thankful and I'll remind of you them occasionally.

      Commands:
      /start -- prints this message
      /help  -- prints this message
      /forgetme  -- deletes you from our system
      /id    -- prints your user id
      /save *note* -- saves a note about something you are thankful for
      """)
  end
end
