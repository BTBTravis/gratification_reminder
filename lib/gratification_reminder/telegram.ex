defmodule GratificationReminder.Telegram do
  alias GratificationReminder.Telegram.Api, as: Api

  defp get_token() do
    Application.get_env(:gratification_reminder, GratificationReminder.Telegram.Api)[:token]
  end

  defp get_base_url() do
    Application.get_env(:gratification_reminder, GratificationReminder.Telegram.Api)[:url]
  end

  def send_message_to_chat(chat_id, msg) do
    Api.client(get_token(), get_base_url())
    |> Api.send_msg(chat_id, msg)
  end
end

