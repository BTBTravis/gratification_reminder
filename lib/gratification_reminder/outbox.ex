defmodule GratificationReminder.Outbox do
  alias GratificationReminder.Telegram, as: Telegram

  def handle_session(session) do
    case session.responces do
      nil -> {:ok, "nothing to send"}
      responces -> Enum.map(responces, message_sender(session.chat_id))
    end
  end

  defp message_sender(chat_id) do
    fn(txt) -> Telegram.send_message_to_chat(chat_id, txt) end
  end
end
