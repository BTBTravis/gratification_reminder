defmodule GratificationReminderWeb.TelegramController do
  use GratificationReminderWeb, :controller

  def send400(conn) do
    conn
    |> send_resp(400, "")
  end

  @doc """
  Handle incoming messages from telegram webhook
  """
  def index(conn, params) do
    case GratificationReminder.Bot.handle_incoming_telegram_msg(params) do
      {:error, _} ->
        send400(conn)
      _ -> json(conn, %{handled: true})
    end
  end
end
