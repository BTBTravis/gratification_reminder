defmodule GratificationReminderWeb.Router do
  use GratificationReminderWeb, :router
  import Plug.Conn

  def put_cors_header(conn, _) do
    Plug.Conn.put_resp_header(conn, "Access-Control-Allow-Origin", "*")
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :put_cors_header
  end

  scope "/api", GratificationReminderWeb do
    pipe_through :api

    post "/message", TelegramController, :index
  end
end
