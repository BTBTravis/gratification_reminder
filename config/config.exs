# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :gratification_reminder,
  ecto_repos: [GratificationReminder.Repo]

# Configures the endpoint
config :gratification_reminder, GratificationReminderWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "kiNa5rJp3a45PbuQL2u6BgLLz6Cizl0qiqh2jHZGMboNVn6gnp6Jp7UQ1fP532sV",
  render_errors: [view: GratificationReminderWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: GratificationReminder.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Telegram API Config
config :gratification_reminder, GratificationReminder.Telegram.Api,
  token: "xxxxxxxxxx",
  url: "https://mockbin.org/echo/"
  #url: "https://www.mocky.io/v2/5185415ba171ea3a00704eed/bot"
#url: https://api.telegram.org/bot

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :tesla, adapter: Tesla.Adapter.Hackney

config :gratification_reminder, GratificationReminder.Telegram.Api,
  http_client: Tesla



# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
